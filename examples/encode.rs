use argh::FromArgs;
use mnemonic::{dictionary, hex_decode, Mnemonic};
use std::{
    error::Error,
    io::{stdin, Read},
};

#[derive(FromArgs, PartialEq, Debug)]
/// CLI arguments.
struct Cli {
    /// mnemonic size.
    #[argh(positional)]
    size: usize,
}

fn main() -> Result<(), Box<dyn Error>> {
    let cli: Cli = argh::from_env();
    let bytes = hex_decode(stdin().bytes().filter_map(Result::ok).map(char::from));
    let mnemonic = Mnemonic::from_entropy(bytes, cli.size)?;
    let dictionary = dictionary::english();
    let words = mnemonic.to_words(&dictionary).collect::<Vec<_>>();
    print!("{}", words.join("-"));
    Ok(())
}
