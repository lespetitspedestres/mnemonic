use mnemonic::{dictionary, hex_encode, Mnemonic};
use std::{
    error::Error,
    io::{stdin, Read},
};

fn main() -> Result<(), Box<dyn Error>> {
    let mut words = String::new();
    stdin().read_to_string(&mut words)?;
    let dictionary = dictionary::english();
    let words = words.split("-").collect::<Vec<_>>();
    let mnemonic = Mnemonic::from_words(&dictionary, words.iter(), words.len())?;
    let entropy = hex_encode(mnemonic.to_entropy()).collect::<String>();
    print!("{}", entropy);
    Ok(())
}
