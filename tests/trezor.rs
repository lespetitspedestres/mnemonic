use hmac::Hmac;
use itertools::Itertools;
use json::JsonValue;
use lazy_static_include::lazy_static_include_str;
use mnemonic::{checksum, dictionary, Mnemonic};
use pbkdf2::pbkdf2;
use sha2::{Sha256, Sha512};
use unicode_normalization::UnicodeNormalization;

fn parse_hex(src: &str) -> Vec<u8> {
    let mut hex = String::with_capacity(2);
    src.chars()
        .chunks(2)
        .into_iter()
        .map(|c| {
            hex.clear();
            hex.extend(c);
            u8::from_str_radix(&hex, 16).unwrap()
        })
        .collect()
}

fn parse_words(src: &str) -> Vec<&str> {
    src.split(' ').collect()
}

fn load_iter(json: &JsonValue) -> impl Iterator<Item = (Vec<u8>, Vec<&str>, Vec<u8>)> {
    json["english"].members().map(|m| {
        (
            parse_hex(m[0].as_str().unwrap()),
            parse_words(m[1].as_str().unwrap()),
            parse_hex(m[2].as_str().unwrap()),
        )
    })
}

fn derive_key(words: &[&str], password: &str) -> Vec<u8> {
    let salt = {
        let mut salt = "mnemonic".to_string();
        salt.push_str(password);
        salt.nfkd().collect::<String>()
    };

    let password = words.join(" ").nfkd().collect::<String>();

    let mut key = vec![0; 64];
    pbkdf2::<Hmac<Sha512>>(password.as_bytes(), salt.as_bytes(), 2048, &mut key);
    key
}

fn size_from_entropy_length(length: usize) -> Option<usize> {
    match length {
        128 => Some(12),
        160 => Some(15),
        192 => Some(18),
        224 => Some(21),
        256 => Some(24),
        _ => None,
    }
}

lazy_static_include_str! {
    TREZOR => "tests/trezor.json",
}

#[test]
fn words() {
    let json = json::parse(&TREZOR).unwrap();
    let dictionary = dictionary::english();
    for (entropy, words, _) in load_iter(&json) {
        let size = size_from_entropy_length(entropy.len() * 8).unwrap();
        let entropy = entropy.iter().copied();
        let entropy = checksum::compute::<Sha256, _>(entropy, size).unwrap();
        let mnemonic = Mnemonic::from_entropy(entropy, size).unwrap();
        assert_eq!(
            mnemonic.to_words(&dictionary).collect::<Vec<_>>(),
            words.as_slice()
        );
    }
}

#[test]
fn keys() {
    let json = json::parse(&TREZOR).unwrap();
    let dictionary = dictionary::english();
    for (entropy, _, key) in load_iter(&json) {
        let size = size_from_entropy_length(entropy.len() * 8).unwrap();
        let entropy = entropy.iter().copied();
        let entropy = checksum::compute::<Sha256, _>(entropy, size).unwrap();
        let mnemonic = Mnemonic::from_entropy(entropy, size).unwrap();
        let words: Vec<_> = mnemonic.to_words(&dictionary).collect();
        assert_eq!(derive_key(&words, "TREZOR"), key.as_slice());
    }
}
