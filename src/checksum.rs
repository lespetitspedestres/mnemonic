//! # Checksum
//!
//! The hasher passed to `compute` and `verify` fails if there is not enough entropy

use crate::{
    error::{Error, Result},
    mnemonic::{checksum_length, entropy_length},
};
use digest::Digest;
use std::iter;

const fn mask(size: usize) -> u8 {
    0xFF << (8 - checksum_length(size))
}

/// Compute and append a checksum byte
pub fn compute<D, E>(entropy: E, size: usize) -> Result<impl Iterator<Item = u8>>
where
    D: Digest,
    E: Iterator<Item = u8> + Clone,
{
    let mut hasher = D::new();

    let checksum = {
        let count = entropy
            .clone()
            .take(entropy_length(size) / 8)
            .inspect(|b| hasher.update(&[*b]))
            .count();
        if count < entropy_length(size) / 8 {
            return Err(Error::NotEnoughEntropy);
        }
        hasher.finalize()[0] & mask(size)
    };

    Ok(entropy.chain(iter::once(checksum)))
}

/// Verify and remove a checksum byte
pub fn verify<D, E>(entropy: E, size: usize) -> Result<impl Iterator<Item = u8>>
where
    D: Digest,
    E: Iterator<Item = u8> + Clone,
{
    let mut hasher = D::new();

    let last = entropy.clone().last().ok_or(Error::NotEnoughEntropy)?;
    let entropy = entropy.take(entropy_length(size) / 8);

    let checksum = {
        if entropy.clone().inspect(|b| hasher.update(&[*b])).count() < entropy_length(size) / 8 {
            return Err(Error::NotEnoughEntropy);
        }
        hasher.finalize()[0] & mask(size)
    };

    if checksum == last {
        Ok(entropy)
    } else {
        Err(Error::BadChecksum)
    }
}
