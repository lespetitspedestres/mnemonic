use std::iter;

/// Encode an iterator of bytes into a iterator of 11 bits values
/// The input iterator can be infinite
pub fn encode(input: impl Iterator<Item = u8>, size: usize) -> impl Iterator<Item = u16> {
    let mut input = input.map(u16::from).peekable();

    (0..size)
        .into_iter()
        .map(move |i| {
            let v = input.next()?;

            let z = ((i + 1) * 5) % 8;
            let m = 0x00FF_u16 >> ((3 * i) % 8);

            let v = match i % 8 {
                2 | 5 => {
                    let v = (v & m) << (8 - z) + 8;
                    v | (input.next()? << (8 - z))
                }
                _ => (v & m) << (8 - z),
            };

            let x = if i % 8 == 7 {
                input.next()?
            } else {
                *input.peek()?
            };

            let v = v | x >> z;

            debug_assert!(v >> 11 == 0);
            Some(v)
        })
        .take_while(Option::is_some)
        .map(Option::unwrap)
}

/// Decode an iterator of 11 bits values into a iterator of bytes
/// The input iterator can be infinite
pub fn decode(input: impl Iterator<Item = u16>, size: usize) -> impl Iterator<Item = u8> {
    let mut t = 0_u16;
    let mut k = 0_u32;
    let mut z = 0_u32;

    let mut o = None;
    let mut input = input.take(size).enumerate();

    iter::from_fn(move || {
        if o.is_some() {
            return o.take();
        }

        if let Some((i, v)) = input.next() {
            debug_assert!(v >> 11 == 0);

            z = (z + 3) % 8;
            k = (k + 3) % 11;

            let b = (v >> k) | t;

            if matches!(i % 8, 2 | 5 | 7) {
                let b = (v >> z) & 0xFF;
                o = Some(b as u8);
                k = (k + 3) % 11;
            }

            let m = 0xFF00_u16.rotate_left(z);
            t = (v & m) << (8 - z);
            debug_assert!(t >> 8 == 0);

            Some(b as u8)
        } else if t == 0 {
            None
        } else {
            let b = t;
            t = 0;
            Some(b as u8)
        }
    })
}

#[cfg(test)]
mod tests {
    use std::iter;

    #[test]
    fn encode() {
        let a: Vec<_> = super::encode(iter::repeat(0b10000000), 24).collect();
        let b: Vec<_> = vec![
            0b10000000100, //
            0b00000100000, //
            0b00100000001, //
            0b00000001000, //
            0b00001000000, //
            0b01000000010, //
            0b00000010000, //
            0b00010000000, //
        ]
        .into_iter()
        .cycle()
        .take(24)
        .collect();
        assert_eq!(a, b);
    }

    #[test]
    fn decode() {
        let a: Vec<_> = super::decode(iter::repeat(0b10000000000), 24).collect();
        let b: Vec<_> = vec![
            0b10000000, //
            0b00010000, //
            0b00000010, //
            0b00000000, //
            0b01000000, //
            0b00001000, //
            0b00000001, //
            0b00000000, //
            0b00100000, //
            0b00000100, //
            0b00000000, //
        ]
        .into_iter()
        .cycle()
        .take(33)
        .collect();
        assert_eq!(a, b);
    }
}
