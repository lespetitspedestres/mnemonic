use std::iter;

pub fn hex_encode(input: impl Iterator<Item = u8>) -> impl Iterator<Item = char> {
    let encode = |b| match b {
        0 => '0',
        1 => '1',
        2 => '2',
        3 => '3',
        4 => '4',
        5 => '5',
        6 => '6',
        7 => '7',
        8 => '8',
        9 => '9',
        10 => 'a',
        11 => 'b',
        12 => 'c',
        13 => 'd',
        14 => 'e',
        15 => 'f',
        _ => unreachable!(),
    };
    input.flat_map(move |b| {
        let h0 = iter::once(encode(b >> 4));
        let h1 = iter::once(encode(b & 0x0F));
        h0.chain(h1)
    })
}

pub fn hex_decode(mut input: impl Iterator<Item = char>) -> impl Iterator<Item = u8> {
    let decode = |c| match c {
        '0' => 0,
        '1' => 1,
        '2' => 2,
        '3' => 3,
        '4' => 4,
        '5' => 5,
        '6' => 6,
        '7' => 7,
        '8' => 8,
        '9' => 9,
        'A' | 'a' => 10,
        'B' | 'b' => 11,
        'C' | 'c' => 12,
        'D' | 'd' => 13,
        'E' | 'e' => 14,
        'F' | 'f' => 15,
        _ => panic!("non-hexadecimal character"),
    };
    iter::from_fn(move || {
        let c0 = input.next()?;
        let c1 = input.next()?;
        let b = (decode(c0) << 4) | decode(c1);
        Some(b)
    })
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand::{thread_rng, Rng};

    #[test]
    fn encode_decode() {
        let mut rng = thread_rng();
        let mut input = vec![0u8; 32];
        rng.fill(input.as_mut_slice());
        let output: Vec<_> = {
            let input = input.iter().copied();
            hex_decode(hex_encode(input)).collect()
        };
        assert_eq!(input, output);
    }
}
