use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error {
    #[error("word '{0}' not found in dictionary")]
    UnknownWord(String),
    #[error("not enough entropy")]
    NotEnoughEntropy,
    #[error("not enough words")]
    NotEnoughWords,
    #[error("bad checksum")]
    BadChecksum,
    #[error("invalid mnemonic size {0}")]
    InvalidSize(usize),
}

pub type Result<T> = std::result::Result<T, Error>;
