use lazy_static_include::lazy_static_include_array;
use std::cmp::Ordering;
use unicode_normalization::{char::is_combining_mark, UnicodeNormalization};

pub const DICTIONARY_INDEX_BITS: usize = 11;
pub const DICTIONARY_SIZE: usize = 1 << DICTIONARY_INDEX_BITS;

pub type Dictionary = [&'static str; DICTIONARY_SIZE];

lazy_static_include_array! {
    /// English dictionary
    DICTIONARY_ENGLISH: [&'static str; DICTIONARY_SIZE] => "dictionary/english.txt",
    /// French dictionary
    DICTIONARY_FRENCH: [&'static str; DICTIONARY_SIZE] => "dictionary/french.txt",
}

pub fn english() -> &'static Dictionary {
    &DICTIONARY_ENGLISH
}
pub fn french() -> &'static Dictionary {
    &DICTIONARY_FRENCH
}

fn normalize(s: &str) -> impl Iterator<Item = char> + '_ {
    s.nfkd().filter(|&c| !is_combining_mark(c))
}

pub fn compare_normalized(a: &str, b: &str) -> Ordering {
    let mut a = normalize(a);
    let mut b = normalize(b);

    loop {
        match (a.next(), b.next()) {
            (Some(a), Some(b)) => match a.cmp(&b) {
                Ordering::Equal => continue,
                o => return o,
            },
            (None, Some(_)) => return Ordering::Less,
            (Some(_), None) => return Ordering::Greater,
            (None, None) => return Ordering::Equal,
        }
    }
}
