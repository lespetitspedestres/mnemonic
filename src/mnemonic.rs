use crate::{
    bits::{decode, encode},
    dictionary::{compare_normalized, Dictionary, DICTIONARY_SIZE},
    error::{Error, Result},
};

#[derive(Debug)]
#[repr(transparent)]
pub struct Mnemonic {
    indices: Box<[u16]>,
}

pub const fn valid_size(size: usize) -> bool {
    matches!(size, 12 | 15 | 18 | 21 | 24)
}

pub const fn total_length(size: usize) -> usize {
    size * 11
}

pub const fn checksum_length(size: usize) -> usize {
    size / 3
}

pub const fn entropy_length(size: usize) -> usize {
    total_length(size) - checksum_length(size)
}

impl Mnemonic {
    pub fn from_entropy(entropy: impl Iterator<Item = u8>, size: usize) -> Result<Self> {
        if !valid_size(size) {
            return Err(Error::InvalidSize(size));
        }

        let indices = encode(entropy, size)
            .inspect(|index| {
                debug_assert!((*index as usize) < DICTIONARY_SIZE);
            })
            .collect::<Vec<_>>();

        if indices.len() < size {
            Err(Error::NotEnoughEntropy)
        } else {
            let indices = indices.into_boxed_slice();
            Ok(Self { indices })
        }
    }

    pub fn from_words<T>(
        dictionary: &Dictionary,
        words: impl Iterator<Item = T>,
        size: usize,
    ) -> Result<Self>
    where
        T: AsRef<str>,
    {
        if !valid_size(size) {
            return Err(Error::InvalidSize(size));
        }

        let indices = words
            .map(|word| {
                let index = dictionary
                    .binary_search_by(|probe| compare_normalized(probe, word.as_ref()))
                    .or(Err(Error::UnknownWord(word.as_ref().into())))?;
                debug_assert!(index < DICTIONARY_SIZE);
                Ok(index as u16)
            })
            .collect::<Result<Vec<_>>>()?;

        if indices.len() < size {
            Err(Error::NotEnoughWords)
        } else {
            let indices = indices.into_boxed_slice();
            Ok(Self { indices })
        }
    }

    pub fn to_entropy(&self) -> impl Iterator<Item = u8> + '_ {
        decode(self.indices.iter().copied(), self.indices.len())
    }

    pub fn to_words<'a>(
        &'a self,
        dictionary: &'static Dictionary,
    ) -> impl Iterator<Item = &'static str> + 'a {
        self.indices
            .iter()
            .copied()
            .map(move |i| dictionary[i as usize])
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{checksum, dictionary};
    use rand::{thread_rng, Rng};
    use sha2::Sha256;

    fn from_words(size: usize) {
        let mut rng = thread_rng();

        let entropy = {
            let mut entropy = vec![0; entropy_length(size) / 8];
            rng.fill(entropy.as_mut_slice());
            checksum::compute::<Sha256, _>(entropy.into_iter(), size).unwrap()
        }
        .collect::<Vec<_>>();

        let mnemonic = Mnemonic::from_entropy(entropy.iter().copied(), size).unwrap();

        let dictionary = dictionary::english();
        let words = mnemonic.to_words(&dictionary);

        let mnemonic = Mnemonic::from_words(&dictionary, words, size).unwrap();

        assert_eq!(entropy, mnemonic.to_entropy().collect::<Vec<_>>());
    }

    #[test]
    fn from_words_12() {
        from_words(12)
    }
    #[test]
    fn from_words_15() {
        from_words(15)
    }
    #[test]
    fn from_words_18() {
        from_words(18)
    }
    #[test]
    fn from_words_21() {
        from_words(21)
    }
    #[test]
    fn from_words_24() {
        from_words(24)
    }
}
